package com.epam;

import com.epam.view.DequeView;
import com.epam.view.PriorityQueueView;

public class App {

    public static void main(String[] args) {
        new DequeView().show();
        new PriorityQueueView().show();
    }
}
