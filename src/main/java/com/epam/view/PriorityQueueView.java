package com.epam.view;

import com.epam.model.queue.PriorityQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PriorityQueueView {

    private static Logger log = LogManager.getLogger(PriorityQueueView.class);

    public void show() {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        priorityQueue.offer(1);
        priorityQueue.offer(2);
        priorityQueue.offer(0);
        priorityQueue.offer(5);
        priorityQueue.offer(3);
        priorityQueue.offer(4);
        log.info("First Element : " + priorityQueue.poll());
        priorityQueue.getQueueAsList().forEach(s -> log.info(s));

    }
}
