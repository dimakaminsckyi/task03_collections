package com.epam.view;

import com.epam.controller.Deque;
import com.epam.model.deque.MyDeque;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DequeView {

    private static Logger log = LogManager.getLogger(DequeView.class);

    public void show() {
        Deque<Integer> deque = new MyDeque<>(5);
        deque.addFirst(2);
        deque.addLast(5);
        deque.addLast(6);
        deque.addFirst(3);
        deque.deleteFirst();
        deque.deleteLast();
        log.debug("Deque first element = " + deque.getFirst());
        log.debug("Deque last element = " + deque.getLast());
    }
}
