package com.epam.controller;

public interface Deque<T> {

    boolean isFull();

    boolean isEmpty();

    boolean addFirst(T o);

    boolean addLast(T o);

    boolean deleteFirst();

    boolean deleteLast();

    T getFirst();

    T getLast();

}
