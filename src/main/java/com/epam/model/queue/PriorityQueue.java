package com.epam.model.queue;

import java.util.*;

public class PriorityQueue<E extends Comparable<E>> extends AbstractQueue<E>{


    private List<E> list = new ArrayList<>();
    private Comparator<E> comparator;

    public PriorityQueue() {
    }

    public PriorityQueue(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean offer(E e) {
        list.add(e);
        if (comparator == null){
            Collections.sort(list);
        }else{
            list.sort(comparator);
        }
        return true;
    }

    @Override
    public E poll() {
        if (!list.isEmpty()){
            E e = list.get(0);
            list.remove(0);
            return e;
        }else{
            return null;
        }

    }

    public List<E> getQueueAsList(){
        return list;
    }

    @Override
    public E peek() {
        if (!list.isEmpty()){
            return list.get(0);
        }
        return null;
    }

    @Override
    public Iterator iterator() {
        return null;
    }
}
