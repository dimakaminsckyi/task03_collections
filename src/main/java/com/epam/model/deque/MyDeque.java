package com.epam.model.deque;

import com.epam.controller.Deque;

public class MyDeque<T> implements Deque<T> {

    private Object arr[];
    private int start;
    private int end;
    private int count;

    public MyDeque(int size) {
        arr = new Object[size];
        start = -1;
        end = -1;
        count = 0;
    }

    @Override
    public boolean isFull() {
        return count == arr.length;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean addFirst(T o) {
        if (isFull()) {
            return false;
        }
        start = (start - 1 + arr.length) % arr.length;
        if (isEmpty()) {
            end = start;
        }
        arr[start] = o;
        count++;
        return true;
    }

    @Override
    public boolean addLast(T o) {
        if (isFull()) {
            return false;
        }
        end = (end + 1) % arr.length;
        if (isEmpty()) {
            start = end;
        }
        arr[end] = o;
        count++;
        return true;
    }

    @Override
    public boolean deleteFirst() {
        if (isEmpty()) {
            return false;
        }
        start = (start + 1) % arr.length;
        count--;
        return true;
    }

    @Override
    public boolean deleteLast() {
        if (isEmpty()) {
            return false;
        }
        end = (end - 1 + arr.length) % arr.length;
        count--;
        return true;
    }

    @Override
    public T getFirst() {
        return isEmpty() ? null : (T) arr[start];
    }

    @Override
    public T getLast() {
        return isEmpty() ? null : (T) arr[end];
    }
}
